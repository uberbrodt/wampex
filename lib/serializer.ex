defmodule Wampex.Serializer do
  @moduledoc "Behaviour for Serializers"
  @type data_type :: :binary | :text
  @callback data_type() :: data_type()
  @callback serialize!(data :: Wampex.message()) :: binary()
  @callback serialize(data :: Wampex.message()) :: {:ok, binary()}
  @callback deserialize!(data :: binary()) :: Wampex.message()
  @callback deserialize(data :: binary()) :: {:ok, Wampex.message()}
end
