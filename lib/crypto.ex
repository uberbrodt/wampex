defmodule Wampex.Crypto do
  @moduledoc false
  def hash_challenge(key, data) do
    :hmac
    |> :crypto.mac(:sha256, key, data)
    |> Base.encode64()
  end

  def pbkdf2(secret, salt, iterations, keylen) do
    {:ok, derived} = :pbkdf2.pbkdf2(:sha256, secret, salt, iterations, keylen)
    Base.encode64(derived)
  end

  def random_string(length) do
    length
    |> :crypto.strong_rand_bytes()
    |> Base.encode64()
  end
end
