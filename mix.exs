defmodule Wampex.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases(),
      deps: deps(),
      docs: [
        main: "readme",
        extras: ["README.md"],
        assets: "assets"
      ]
    ]
  end

  defp elixirc_paths(_), do: ["lib"]

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.2", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 0.5.1", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:jason, "~> 1.1"},
      {:msgpax, "~> 2.2"},
      {:pbkdf2, "~> 2.0"}
    ]
  end

  defp aliases do
    [
      credo: "credo --strict",
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end
